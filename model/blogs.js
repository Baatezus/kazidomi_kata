export const blogs = [
    {
        id: 1,
        title: 'Boissons d\'Amandes',
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mollis efficitur augue et consectetur. Nulla molestie mi metus, at elementum enim ultrices eu. Pellentesque commodo mauris ac tempor commodo. Integer maximus augue arcu, vel volutpat dolor molestie sed. Etiam tempor vitae sapien id consectetur. Donec sapien enim, dictum in rhoncus quis, molestie sed nisi. Sed vitae nisi non mauris facilisis scelerisque. Aliquam erat volutpat. Ut accumsan libero vel justo pretium, nec consectetur eros facilisis.',
        imagePath: 'https://images-na.ssl-images-amazon.com/images/I/51ODBfoqozL.jpg'
    },
    {
        id: 2,
        title: 'Santé et minéraux',
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mollis efficitur augue et consectetur. Nulla molestie mi metus, at elementum enim ultrices eu. Pellentesque commodo mauris ac tempor commodo. Integer maximus augue arcu, vel volutpat dolor molestie sed. Etiam tempor vitae sapien id consectetur. Donec sapien enim, dictum in rhoncus quis, molestie sed nisi. Sed vitae nisi non mauris facilisis scelerisque. Aliquam erat volutpat. Ut accumsan libero vel justo pretium, nec consectetur eros facilisis.',
        imagePath: 'https://cdn.france-mineraux.fr/wp-content/uploads/2015/02/pierre-amethyste-700x480.jpg'
    },
    {
        id: 3,
        title: 'Les huiles essentielles',
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mollis efficitur augue et consectetur. Nulla molestie mi metus, at elementum enim ultrices eu. Pellentesque commodo mauris ac tempor commodo. Integer maximus augue arcu, vel volutpat dolor molestie sed. Etiam tempor vitae sapien id consectetur. Donec sapien enim, dictum in rhoncus quis, molestie sed nisi. Sed vitae nisi non mauris facilisis scelerisque. Aliquam erat volutpat. Ut accumsan libero vel justo pretium, nec consectetur eros facilisis.',
        imagePath: 'https://bioflore.be/1106-large_default/huile-essentielle-de-copahier-ou-copahu.jpg'
    }
]