import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Home from "../components/pages/Home"
import Blog from "../components/pages/Blog"
import Logo from "../components/shared/Logo"
import { globalStyles } from "../styles/global"

const Stack = createStackNavigator();

const Routes = () => {
    return(
        <Stack.Navigator>
            <Stack.Screen
                name="Home"
                component={Home}
                options={{...headerOptions, headerTitle: () => <Logo/>}}
            />
            <Stack.Screen
                name="Details"
                component={Blog}
                options={{...headerOptions, title: 'Blog details'}}
            />
        </Stack.Navigator>
    )
}

const headerOptions = {
    headerStyle: globalStyles.header,
    headerTintColor: '#fff',
    title: 'Kazidomi kata',
}

export default Routes