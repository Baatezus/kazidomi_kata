import { StyleSheet } from 'react-native'

export const globalStyles = StyleSheet.create({
    container: {
        margin: 8,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    title: {
        color: '#23adad',
        fontSize: 24
    },
    blogImage: {
        marginTop: 8,
        marginBottom: 8,
        width: '100%',
        height: 300
    },
    card: {
        borderRadius: 6,
        elevation: 3,
        backgroundColor: '#fff',
        shadowOffset: { width: 1, height: 1 },
        shadowColor: '#333',
        shadowOpacity: 0.3,
        shadowRadius: 2,
        marginHorizontal: 4,
        marginVertical: 6,
    },
    cardContent: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginHorizontal: 18,
        marginVertical: 20,
    },
    thumb: {
        height: 40,
        width: 40
    },
    logoContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    logo: {
        width: 100,
        height: 45
    },
    header: {
        backgroundColor: '#23adad',
    }
})