import {blogs} from "../../model/blogs"
import {sendFakeNotification} from "../../notifications/notificationHandler"

export const getAllBlogs = () => {
    const data = blogs
    return {type: 'GET_ALL', data}
}

export const fakeNotification = navigation => sendFakeNotification(navigation)
