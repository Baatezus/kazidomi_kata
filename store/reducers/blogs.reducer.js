import { combineReducers } from 'redux';

const initState = {
    blogs: [],
};

const blogReducer = (state = initState, action) => {
    switch (action.type) {
        case 'GET_ALL':
            return {...state, blogs: action.data}
        case 'DELETE':
            return {...state, blogs: state.blogs.filter(blog => blog.id !== action.id)}
        default:
            return state
    }
};

export default combineReducers({
    blogs: blogReducer
});