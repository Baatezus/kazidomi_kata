import React from 'react'
import { Text, View, Image } from 'react-native'
import { globalStyles } from "../../styles/global"


const Blog = ({route}) => {
    return (
        <View style={globalStyles.container}>
            <Text style={globalStyles.title}>{route.params.title}</Text>
            <Image style={globalStyles.blogImage}  source={{uri: route.params.imagePath}}/>
            <Text>{route.params.content}</Text>
        </View>
    )
}

export default Blog