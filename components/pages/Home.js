import React, {useEffect} from 'react'
import { connect } from 'react-redux'
import { View, TouchableOpacity, FlatList, Button } from 'react-native'
import {fakeNotification, getAllBlogs} from "../../store/actions/blogs.actions"
import BlogCard from "../atoms/BlogCard"
import { globalStyles } from "../../styles/global"
import {sendFakeNotification} from "../../notifications/notificationHandler"




const Home = (
    {
        navigation,
        blogs,
        getAllBlogs,
        fakeNotification
    }
    ) => {

    useEffect(() => { getAllBlogs() } , [])

    return (
        <View style={globalStyles.container}>
            <FlatList
                data={blogs}
                keyExtractor={item => item.title}
                renderItem={({ item }) => (
                    <TouchableOpacity   onPress={() => navigation.navigate('Details', item)}>
                        <BlogCard blog={item} />
                    </TouchableOpacity>
                )}
            />
            <Button
                onPress={() => sendFakeNotification(navigation)}
                title="Send fake notification"
                color="#23adad"
                accessibilityLabel="Send fake notification"
            />
        </View>
    )
}


const mapStateToProps = state => {
    return {
        blogs: state.blogs.blogs
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getAllBlogs: () => dispatch( getAllBlogs() ),
        fakeNotification: navigation => dispatch( fakeNotification(navigation) )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)