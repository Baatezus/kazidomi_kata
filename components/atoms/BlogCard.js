import React from 'react'
import { View, Text, Image } from 'react-native'
import { globalStyles } from "../../styles/global"

const BlogCard = ({blog}) => {
    return (
        <View style={globalStyles.card}>
            <View style={globalStyles.cardContent}>
                <Image source={{uri: blog.imagePath}}  style={globalStyles.thumb}/>
                <Text>{blog.title}</Text>
            </View>
        </View>
    )
}

export default BlogCard