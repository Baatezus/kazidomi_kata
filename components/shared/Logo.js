import React from 'react'
import { Image, View } from 'react-native'
import { globalStyles } from "../../styles/global"

const Logo = () => {
    return (
        <View style={globalStyles.logoContainer}>
            <Image style={globalStyles.logo} source={require('../../assets/logo_kazidomi_white.png')} />
        </View>
    )
}


export default Logo