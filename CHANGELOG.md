# Kazidomi Kata changelog

## Added
- Add the app header
- Add navigation
- Add global state management tools (redux)
- Add Blog class and fake data getter
- Add blog list view
- Add blog details view
- Add Kazidomi logo
- Add notifications handler
- Add fake notification sender
- Add README.md

## Modified
- All styles sheets in global file
- Eject project from Expo to generate Android & iOs folders 