# KAZIDOMI KATA

## PREREQUISITE

To test the application, you will need severals tools: 
- Android Studio to run an virtual device(https://developer.android.com/studio)
- Node Js v12.19.0, React native is NOT compatible with higher version(https://nodejs.org/en/download/)
- Expo to run the project. Using npm run this command line: ``npm install expo-cli --global``

## GETTING STARTED
### Clone this repository

``git clone https://Baatezus@bitbucket.org/Baatezus/kazidomi_kata.git``

### Installing dependencies
At the root of the project run this command line:

``npm install``

## RUNNING THE APP
- Open the Android Studio AVD manager and run a virtual device.
- At the root of the project run 

``expo start``

The default navigator will open a Expo web page, wait for the QR code to show, then click on the ``Run on Android device/emulator`` button.

##APP FEATURES
The demo app has three features: 
- Home page, showing a list of blogs
- Blog details page, you can access to this page by clicking on one of the list's item.
- Send notification by pressing on the home page button ``Send fake notification``

## KNOWN ISSUES
- ``Error Failed to launch emulator. Reason: No emulators found as an output of `emulator -list-avds``
It is preferable to launch AVD manager without an opened project on Android Studio to prevent that error.



- ``TypeError: null is not an object (evaluating 'RNPushNotification.getInitialNotification')
``
Because Expo, React Native and  especially the notification systems relies on a lot of different npm packages, make sure to have the latest version of each packages that they depending on.

