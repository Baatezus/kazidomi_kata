import React from 'react'
import { StatusBar  } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './store/reducers/blogs.reducer'
import Routes from "./routes/Routes"


const store = createStore(reducer)

export default function App() {
  return (
      <Provider store={store}>
          <StatusBar backgroundColor='#018b8b' />
          <NavigationContainer>
              <Routes/>
          </NavigationContainer>
      </Provider>
  )
}
