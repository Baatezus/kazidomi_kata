import PushNotification from "react-native-push-notification"
import {blogs} from "../model/blogs"


export const sendFakeNotification = navigation => {
    PushNotification.configure({
        permissions: {
            alert: true,
            badge: true,
            sound: true,
        },
        popInitialNotification: false,
        requestPermissions: true,
    });

    PushNotification.localNotification({
        onNotification: notification => {
            navigation.navigate('Details', blogs[0])
            notification.finish()
        },
        title: "New blog !!!",
        message: "Boissons d'amandes",
        soundName: "default",
        number: 10,
        repeatType: "day",
    });
}

